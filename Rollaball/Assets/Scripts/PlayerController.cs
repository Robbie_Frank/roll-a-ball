﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System;

public class PlayerController : MonoBehaviour
{
    // Variables
    float movementX;
    float movementY;
    int count = 16;
    [SerializeField] float speed = 0;
    [SerializeField] float boostForce = 0;
    bool usedBoost = false;
    Timer timer = new Timer(3.0f);

    // References
    Rigidbody rb;
    [SerializeField] GameObject winTextObject;
    [SerializeField] GameObject loseTextObject;
    [SerializeField] TextMeshProUGUI timerText;
    [SerializeField] GameObject offMapTextObject;
    [SerializeField] GameObject retryButton;
    [SerializeField] GameObject quitButton;
    [SerializeField] GameObject gameController;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        winTextObject.SetActive(false);
        loseTextObject.SetActive(false);
        offMapTextObject.SetActive(false);
        retryButton.SetActive(false);
        quitButton.SetActive(false);
    }

    // Handles player movement
    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;
    }
    void OnBoost()
    {
        if (!usedBoost)
        {
            usedBoost = true;
            Vector3 movement = new Vector3(movementX, 0, movementY);
            rb.AddForce(movement * boostForce, ForceMode.Impulse);
        }
    }

    // Determines if the player has won or lost and displays the appropriate text. Also pauses the game.
    void WinOrLose()
    {
        if (timer.currentTime <= 0f && count > 0)
        {
            loseTextObject.SetActive(true);
            retryButton.SetActive(true);
            quitButton.SetActive(true);
            Time.timeScale = 0;
        }
        if (timer.currentTime > 0f && count == 0)
        {
            winTextObject.SetActive(true);
            retryButton.SetActive(true);
            quitButton.SetActive(true);
            Time.timeScale = 0;
        }
    }

    private void Update()
    {
        // Removes time from timer in seconds
        timer.CountDown();
        // Updates the timerText to display the remaining time on screen in the language that was chosen from the main menu
        if (gameController.GetComponent<LanguageChoice>().englishSelected)
        {
            timerText.text = "Time: " + Math.Truncate(timer.currentTime);
        }
        else if (gameController.GetComponent<LanguageChoice>().spanishSelected)
        {
            timerText.text = "Tiempo: " + Math.Truncate(timer.currentTime);
        }
        // Checks to see if the win or lose condition has been met
        WinOrLose();
        // Checks to see if player presses the spacebar, if they do, they get a boost in speed.
    }

    private void FixedUpdate()
    {
        // Moves the player
        Vector3 movement = new Vector3(movementX, 0, movementY);
        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            // Makes pickup object disappear when touched
            other.gameObject.SetActive(false);
            // Decreases the count variable by 1
            count--;
            // Adds time to the timer
            timer.currentTime += 1.0f;
        }
        // Runs if player falls off the map. Pauses the game and displays the "fell off map" text
        if (other.gameObject.name == "Off Map")
        {
            offMapTextObject.SetActive(true);
            retryButton.SetActive(true);
            quitButton.SetActive(true);
            Time.timeScale = 0;
        }
    }
}