﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer
{
    // Variables
    public float currentTime { get; set; }

    // Constructor
    public Timer(float startTime)
    {
        currentTime = startTime;
    }

    // Removes time from the timer
    public void CountDown()
    {
        if (currentTime > 0f)
        {
            currentTime -= 1 * Time.deltaTime;
        }
        else currentTime = 0f;
    }

    // Adds time to timer
    public void CountUp()
    {
        currentTime += 1 * Time.deltaTime;
    }
}