﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardController : MonoBehaviour
{
    // Variables
    [SerializeField] float maxTiltAngle;
    [SerializeField] float rotateSpeedX;
    [SerializeField] float rotateSpeedY;
    [SerializeField] float rotateSpeedZ;

    // Start is called before the first frame update
    void Start()
    {
        ChooseDirection();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(rotateSpeedX, rotateSpeedY, rotateSpeedZ) * Time.deltaTime);
    }

    // Randomly determines axis and direction to tilt/spin the board
    void ChooseDirection()
    {
        // Randomly generates a number 0-2 to be used to determine which axis to rotate on. 0 = x, 1 = y, 2 = z
        int axis = Random.Range(0, 3);
        // Randomly generates a number 0-1 to be used to determine the direction of rotation. 0 = left, 1 = right
        int direction = Random.Range(0, 2);
        switch (axis)
        {
            // Spins table around x axis
            case 0:
                rotateSpeedX = -5f;
                break;
            // Tilts table on y axis
            case 1:
                if (direction == 0)
                {
                    rotateSpeedY = -50f;
                }
                else
                {
                    rotateSpeedY = 50f;
                }
                break;
            // Tilts table on z axis
            case 2:
                if (direction == 0)
                {
                    rotateSpeedZ = 5f;
                }
                else
                {
                    rotateSpeedZ = -5f;
                }
                break;
        }
    }
}