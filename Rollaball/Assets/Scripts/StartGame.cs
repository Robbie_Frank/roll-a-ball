﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    // References
    [SerializeField] GameObject mainMenu;

    public void BeginGame()
    {
        mainMenu.SetActive(false);
        Time.timeScale = 1;
    }
}