﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class LanguageChoice : MonoBehaviour
{
    // Variables
    public bool englishSelected = true;
    public bool spanishSelected = false;
    string newLine = Environment.NewLine;

    // References
    [SerializeField] GameObject winTextObject;
    [SerializeField] GameObject loseTextObject;
    [SerializeField] GameObject timerText;
    [SerializeField] GameObject offMapTextObject;
    [SerializeField] GameObject retryButton;
    [SerializeField] GameObject startButton;
    [SerializeField] GameObject controlsText;
    [SerializeField] GameObject quitText;

    private void Awake()
    {
        Time.timeScale = 0;
    }

    // Changes all in-game text to Spanish
    public void changeToSpanish()
    {
        // Checks to see if things are already in Spanish, if not, translates everything to Spanish
        if (!spanishSelected)
        {
            spanishSelected = true;
            englishSelected = false;
            winTextObject.GetComponentInChildren<TextMeshProUGUI>().text = "¡Tú ganas!";
            loseTextObject.GetComponentInChildren<TextMeshProUGUI>().text = "¡Te quedaste sin tiempo antes de recoger todos los objetos!";
            offMapTextObject.GetComponentInChildren<TextMeshProUGUI>().text = "¡Te caíste de la mesa!";
            retryButton.GetComponentInChildren<TextMeshProUGUI>().text = "¡Juega De Nuevo!";
            startButton.GetComponentInChildren<TextMeshProUGUI>().text = "Empezar Juego";
            timerText.GetComponentInChildren<TextMeshProUGUI>().text = "Tiempo: ";
            controlsText.GetComponentInChildren<TextMeshProUGUI>().text = "Controles:" + newLine + "Movimiento: WASD" + newLine + "Impulso: Barra espaciadora (¡Solo puedes impulsar una vez!)" + newLine + "Recoge todos los cubos rojos antes de que acabe el tiempo. Obtienes más tiempo por cada cubo que recolectas. ¡No te caigas del mapa!";
            quitText.GetComponentInChildren<TextMeshProUGUI>().text = "Salir del Juego";
        }
    }

    // Changes all in-game text to English
    public void changeToEnglish()
    {
        // Checks to see if things are already in English, if not, translates everything to English
        if (!englishSelected)
        {
            englishSelected = true;
            spanishSelected = false;
            winTextObject.GetComponentInChildren<TextMeshProUGUI>().text = "You win!";
            loseTextObject.GetComponentInChildren<TextMeshProUGUI>().text = "You ran out of time before collecting all the objects!";
            offMapTextObject.GetComponentInChildren<TextMeshProUGUI>().text = "You fell off the table!";
            retryButton.GetComponentInChildren<TextMeshProUGUI>().text = "Play Again!";
            startButton.GetComponentInChildren<TextMeshProUGUI>().text = "Start Game";
            timerText.GetComponentInChildren<TextMeshProUGUI>().text = "Time: ";
            controlsText.GetComponentInChildren<TextMeshProUGUI>().text = "Controls:" + newLine + "Movement: WASD" + newLine + "Boost: Spacebar (You can only boost one time!)" + newLine + "Collect all the red cubes before time runs out. You get more time for each cube you collect. Don't fall off the map!";
            quitText.GetComponentInChildren<TextMeshProUGUI>().text = "Quit Game";
        }
    }
}